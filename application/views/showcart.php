<h2>Daftar Pemesanan Film</h2>
<form action="<?=base_url('index.php/Cart/simpan')?>" method="post">
<table class="table table-hover table-striped">
	<tr>
		<td>Nama Film</td>
		<td>Kategori</td>
		<td>Qty</td>
		<td>Harga</td>
		<td>Subtotal</td>
		<td>Aksi</td>
	</tr>

	<?php 
		foreach($this->Cart->contents() as $items){
	?>

	<tr>
		<td>
			<input type="hidden" name="idfilm[]" value="<?=$items['id']?>">
			<input type="hidden" name="qty[]" value="<?=$items['qty']?>">
			<?= $items['name']?>
		</td>
		<td><?= $items['options']['kategori']?></td>
		<td><?= $items['qty']?></td>
		<td><?= $items['price']?></td>
		<td><?= $items['subtotal']?></td>
		<td>
			<a href="<?=base_url('index.php/Cart/hapuscart/'.$items['rowid'])?>" onclick="return confirm('r u sure?')">Hapus</a>
		</td>
	</tr>

	<?php
	}
	?>

	<tr>
		<input type="hidden" name="grand_total" value="<?=$this->Cart->total()?>">
		<td colspan="4">Grand Total</td><td><?= $this->cart->total()?></td>
		<td></td>
	</tr>
</table>
<input type="submit" name="simpan" value="CHECK-OUT" class="btn btn-success">
</form>