<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale-1">
	<title>BIOSKOP</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>aset/bioskop/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>aset/bioskop/css/style.css">

	<script type="text/javascript" src="<?=base_url();?>aset/bioskop/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>aset/bioskop/js/bootstrap.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
  		<div class="container-fluid">
			<div class="navbar-header">
  				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
  				<a class="navbar-brand" href="<?=base_url();?>index.php/Hello/index"><b>Cinema 21</b></a>
	  		</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    			<ul class="nav navbar-nav navbar-right">
    				<form class="navbar-form navbar-left">
				        <div class="form-group">
				          <input type="text" class="form-control" placeholder="Search">
				        </div>
				        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				    </form>
    				<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          					<span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('username');?><span class="caret"></span>
          				</a>
          				<ul class="dropdown-menu">
				            <li><a href="<?=base_url();?>index.php/Hello/account">Account</a></li>
                    <li>
                      <a href="<?=base_url();?>index.php/Hello/tampilposter">Schedule</a>
                    </li>
				            <li><a href="<?=base_url();?>index.php/Hello/troli">Troli</a></li>
				            <li><a href="<?=base_url();?>index.php/Hello/logout"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
          				</ul>
        			</li>
    			</ul>
    		</div>
  		</div>
	</nav>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  	<!-- Indicators -->
  		<ol class="carousel-indicators">
    		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
  		  	<li data-target="#carousel-example-generic" data-slide-to="1"></li>
    		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	  	</ol>

  	<!-- Wrapper for slides -->
  		<div class="carousel-inner" role="listbox">
    		<div class="item active">
      			<img src="<?=base_url();?>aset/bioskop/picture/1.jpg" alt="muehe">
      			<div class="carousel-caption">
      			</div>
    		</div>
    		<div class="item">
      			<img src="<?=base_url();?>aset/bioskop/picture/2.jpg" alt="...">
      			<div class="carousel-caption">
     	 		</div>
    		</div>
    		<div class="item">
      			<img src="<?=base_url();?>aset/bioskop/picture/3.jpg" alt="...">
      			<div class="carousel-caption">
     	 		</div>
    		</div>
  		</div>

  	<!-- Controls -->
  		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    		<span class="sr-only">Previous</span>
  		</a>
  		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    		<span class="sr-only">Next</span>
  		</a>
	</div>

  <div class="container">
    <div class="row">
      <?php
        $this->load->view($konten);
      ?>
    </div>
  </div>

  <div class="bawah">
        <a>About</a>
        <a>Help</a>
        <a>Privacy & Terms</a>
    </div>


  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <center>
          <div class="input-group ">
              <span class="input-group-addon" id="sizing-addon1">Studio</span>
              <input type="text" class="form-control" placeholder="Studio" aria-describedby="sizing-addon1">
          </div>
          <div class="input-group ">
              <span class="input-group-addon" id="sizing-addon1">Kursi</span>
              <input type="text" class="form-control" placeholder="Nomor Kursi" aria-describedby="sizing-addon1">
          </div>
          <div class="troli">
            <img src="<?=base_url();?>aset/bioskop/picture/troli.png">
          </div>
        </center>
      </div>
    </div>
  </div>
</body>
</html>
<script>
  $(document).ready(function(){
    var navigasi=$(".carousel").offset().top;
    var sticky=function(){
      var scrolltop=$(window).scrollTop();
      if(scrolltop>navigasi){
        $(".navbar-inverse").addClass("fix");
      }
      else{
        $(".navbar-inverse").removeClass("fix");
      }
    } 
    sticky();
    $(window).scroll(function(){
      sticky();
    });
  });
</script>