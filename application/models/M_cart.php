<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cart extends CI_Model {

	public function simpan_cart()
	{
	$object=array(
		'grandtotal'=>$this->input->post('grandtotal'),
		'idpembeli'=>$this->session->userdata('idpembeli'),
		'tgl_beli'=>date('Y-m-d'),
		'bukti'=>''
		);

		$mk_nota = $this->db->insert('nota', $object);

		if ($mk_nota) {
			$tm_nota=$this->db->order_by('idnota','desc')->limit(1)->get('nota')->row();
			for($i=0;$i<count($this->input->post('idfilm'));$i++){
				$hasil[]=array(
					'idnota'=>$tm_nota->idnota,
					'idfilm'=>$this->input->post('idfilm')[$i],
					'grandtotal'=>$this->input->post('qty')[$i]
					);

			$proses = $this->db->insert_batch('tiket', $hasil);
			return $tm_nota->idnota;
			}
		} else {
			return 0; // 0 / FALSE
		}
		
	}
	public function getnota($id)
	{
		return $this->db->where('idnota', $id)
						->get('nota')->row();
	}
	

}