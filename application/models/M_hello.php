<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_hello extends CI_Model {

	public function dataposter(){
		return $this->db->get('film')->result();
	}

	public function detaildata($id){
		return $this->db->where('idfilm',$id)
						//->join('kategori','film.idkategori=kategori.idkategori')
						->get('film')->row();
	}

	public function simpan_data()
	{
		$nama=$this->input->post('nama');
		$email=$this->input->post('email');
		$telp=$this->input->post('telp');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$data=array(
			'nama'=>$nama,
			'email'=>$email,
			'telp'=>$telp,
			'username'=>$username,
			'password'=>md5($password)
		);
		return $this->db->insert('pembeli',$data);
	}

	public function cek_user(){
		return $this->db->where('username',$this->input->post('username'))
				->where('password',md5($this->input->post('password')))
				->get('pembeli');
	}
}