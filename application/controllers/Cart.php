<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function index()
	{
		$data['konten']="showcart";
		$this->load->view('template', $data, FALSE);
	}

	public function addcart($idfilm)
	{
		$this->load->models('M_hello');
		$detail=$this->M_hello->detaildata($idfilm);
		$data=array(
			'id' 	=> $detail->idfilm,
			'qty'	=> 1,
			'price'	=> 2000,
			'name'	=> $detail->namafilm,
			'options'	=>array('genre'=>$detail->kategori)
		);

		$this->Cart->insert($data);
		redirect('Hello/detailfilm/'.$idfilm,'refresh');
	}

	public function hapuscart($id)
	{
		$data = array(
			'rowid' => $id,
			'qty'	=> 0
		);

		$this->cart->update($data);
		redirect('cart','refresh');
	}

	public function simpan()
	{
		if($this->input->post('simpan')) {
			$this->load->model('M_cart');
			$id_nota=$this->model_cart->simpancart();
			if($id_nota > 0) {
				$this->cart->destroy();
				redirect('Cart/pembayaran/'.$idnota, 'refresh');
			} else {
				redirect('cart', 'refresh');
			}
		}
	}

	function pembayaran ($id)
	{
		$this->load->model('M_cart');
		$nota=$this->m_cart->getnota($id);
		$data['total']=$nota->grandtotal+$id;
		$data['konten']="v_pembayaran";
		$data['id_nota']=$id;
		$this->load->view('template',$data,FALSE);
	}

	public function konfirm($id_nota='')
	{
		$data['konten']="v_konfirm";
		$this->load->view('template',$data,FALSE);
	}

	public function upload()
	{
		if($this->input->post('simpan')){
			$config['upload_path'] = 'aset/bukti_pembayaran/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('foto')){
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('pesan', $error);
				redirect('cart/confirm/'.$this->input->post('idnota'),'refresh');
			} else {
				$this->load->model('M_cart','pesan');
				$proses = $this->pesan->save_db($this->input->post('idnota'),$this->upload->data('file_name'));
				if ($proses) {	
					$this->session->set_flashdata('pesan', 'Sukses mengirim bukti. Mohon Tunggu Konfirmasi dari Admin');
					redirect('cart/confirm/'.$this->input->post('idnota'),'refresh');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal mengirim bukti');
					redirect('cart/confirm/'.$this->input->post('idnota'),'refresh');
				}
				
			}
		}
	}
}
