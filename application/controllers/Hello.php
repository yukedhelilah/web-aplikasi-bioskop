<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('login')!=TRUE){
			redirect('Hello/login','refresh');
		}
		$data['judul']="Home";
		$data['konten']="Home";
		$this->load->view('template',$data,FALSE);
	}

	public function account()
	{
		$data['judul']="account";
		$data['konten']="account";
		$this->load->view('template',$data,FALSE);
	}

	public function schedule()
	{
		$data['judul']="schedule";
		$data['konten']="schedule";
		$this->load->view('template',$data,FALSE);
	}

	public function troli()
	{
		$data['judul']="troli";
		$data['konten']="troli";
		$this->load->view('template',$data,FALSE);
	}	

	public function tampilposter()
	{
		$this->load->model('M_hello');
		$data['tampilposter']=$this->M_hello ->dataposter();
		$data['konten'] = "schedule";
		$this->load->view('template',$data);
	}

	public function detailfilm($idfilm)
	{
		$this->load->model('M_hello');
		$data['tampildetail']=$this->M_hello->detaildata($idfilm);
		$data['konten'] = "detail";
		$this->load->view('template',$data);
	}

	public function login()
	{
		if($this->session->userdata('login')==TRUE){
			redirect('hello','refresh');
		}
		else{
			$this->load->view('login');
		}
	}

	public function signup()
	{
		$this->load->view('signup');
	}

	public function simpan()
	{
		if($this->input->post('submit'))
		{
		$this->form_validation->set_rules('nama','nama','trim|required');
		$this->form_validation->set_rules('email','email','trim|required');
		$this->form_validation->set_rules('telp','no telp','trim|required');
		$this->form_validation->set_rules('username','username','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');

			if ($this->form_validation->run()==True)
			{
				$this->load->model('M_hello');
				$proses=$this->M_hello->simpan_data();

				if ($proses)
				{
					$this->session->set_flashdata('pesan', 'sukses simpan');
					redirect('Hello/login');
				}
				else
				{
					$this->session->set_flashdata('pesan', 'gagal simpan');
					redirect('Hello/signup');	
				}
			}

			else
			{
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('Hello/signup');
			}
		}
	}

	public function masuk()
	{
		if($this->input->post('cek')){
			$this->form_validation->set_rules('username','username','trim|required');
			$this->form_validation->set_rules('password','password','trim|required');
			if ($this->form_validation->run() == TRUE) {		
				$this->load->model('M_hello');					
				if ($this->M_hello->cek_user()->num_rows()>0) {					
					$data_user=$this->M_hello->cek_user()->row();				
					$array = array(
						'login' => TRUE,
						'username' => $data_user->username,
						'password' => $data_user->password,
						'nama' => $data_user->nama,
					);

					$this->session->set_userdata( $array );
					redirect('hello','refresh');
				}

				else{
					$this->session->set_flashdata('pesan', 'username dan password salah');
					redirect('hello/login','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan',validation_errors());
				redirect('hello/login','refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Hello/login','refresh');
	}
}