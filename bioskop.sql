-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2018 at 03:23 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bioskop`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `idfilm` int(11) NOT NULL,
  `idkategori` int(11) NOT NULL,
  `namafilm` varchar(50) DEFAULT NULL,
  `desfilm` text,
  `poster` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`idfilm`, `idkategori`, `namafilm`, `desfilm`, `poster`) VALUES
(1, 0, 'Aku, Benci, & Cinta', 'Film remaja berlatarkan SMA yang sangat cocok ditonton remaja saat ini', 'abc.jpg'),
(2, 0, 'Koala Kumal', 'Film karya raditya dika yang diangkat dari novel buatannya sendiri', 'k.jpg'),
(3, 0, 'Mereka Yang Tak Terlihat', 'Film Horor yang akan menakuti pemirsa yang akan menontonnya', 'm.jpg'),
(4, 0, 'Warkop DKI Reborn', 'Film tahun 90 an yang dikemas dengan warna baru', 'w.jpg'),
(5, 0, 'Promise', 'Film yang mengambil lokasi syuting di Italia', 'p.jpg'),
(6, 0, 'One Fine Day', 'Film yang diambil di spanyol dengan pemeran utama artis pendatang baru', 'o.jpg'),
(7, 0, 'Cek Tokoh Sebelah', 'Film karya alumni Stand Up Comedy season 2, Ernest ', 'c.jpg'),
(8, 0, 'Hangout', 'Film horor yang akan diwarnai dengan genre baru yaitu dengan bumbu komedi', 'h.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `idjadwal` int(11) NOT NULL,
  `tgltayang` date DEFAULT NULL,
  `jamtayang` time NOT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`idjadwal`, `tgltayang`, `jamtayang`, `harga`) VALUES
(1, '2017-09-25', '13:00:00', 30),
(2, '2017-09-26', '15:00:00', 30),
(3, '2017-09-27', '17:00:00', 30),
(4, '2017-09-28', '19:00:00', 30),
(5, '2017-09-29', '21:00:00', 35),
(6, '2017-09-30', '13:00:00', 40),
(7, '2017-10-01', '15:00:00', 40);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `idkategori` int(11) NOT NULL,
  `namakategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kursi`
--

CREATE TABLE `kursi` (
  `idkursi` int(11) NOT NULL,
  `nokursi` varchar(10) DEFAULT NULL,
  `idstudio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kursi`
--

INSERT INTO `kursi` (`idkursi`, `nokursi`, `idstudio`) VALUES
(1, '1A', 1),
(2, '2A', 2),
(3, '3A', 3),
(4, '4A', 4),
(5, '5A', 5),
(6, '1B', 2);

-- --------------------------------------------------------

--
-- Table structure for table `nonton`
--

CREATE TABLE `nonton` (
  `idnonton` int(11) NOT NULL,
  `idstudio` int(11) DEFAULT NULL,
  `idfilm` int(11) DEFAULT NULL,
  `idjadwal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nonton`
--

INSERT INTO `nonton` (`idnonton`, `idstudio`, `idfilm`, `idjadwal`) VALUES
(1, 3, 1, 4),
(2, 2, 2, 7),
(3, 5, 1, 6),
(4, 1, 4, 2),
(5, 4, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `idnota` int(11) NOT NULL,
  `idpembeli` int(11) NOT NULL,
  `tgl_beli` date NOT NULL,
  `bukti` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `idpembeli` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telp` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`idpembeli`, `foto`, `nama`, `email`, `telp`, `username`, `password`) VALUES
(1, '', '', '', 0, '', ''),
(2, '', '', '', 0, '', ''),
(3, '', '', '', 0, '', ''),
(4, '', '', '', 0, '', ''),
(5, '', 'yuke', 'yukedhelilah@gmail.com', 12345678, 'yukedhelilah', '123'),
(6, '', 'nama', 'email', 12345678, 'username', '5f4dcc3b5a'),
(7, '', 'u', 'u', 0, 'u', '7b774effe4'),
(8, '', 'YUKE DHELILAH NUR HIDAYATI', 'yuke_hidayati_25rpl@student.smktelkom-mlg.sch.id', 98786543, 'vani', '202cb962ac'),
(9, '', 'Irfan', 'irfan', 2147483647, 'irfan', '202cb962ac'),
(10, '', 'Irfan', 'hakim', 2147483647, 'irfanhkm', '202cb962ac'),
(11, '', 'Irfan', 'irfan', 2147483647, 'irfan', '202cb962ac59075b964b07152d234b70'),
(12, '', 'yuke', 'yukedhelilah@gmail.com', 1246890, 'yuke', '202cb962ac59075b964b07152d234b70'),
(13, '', 'funi', 'funi', 0, 'funi', 'caa4bb6464bb41b69222e918a6c15a33'),
(14, '', 'Yuke Dhelilah', 'yukedhelilah@gmail.com', 2147483647, 'yukedhelilah', '202cb962ac59075b964b07152d234b70'),
(15, '', 'kuy', 'kuy', 0, 'kuy', 'ea2fd9732bf0a85522e8c11ce00d3371'),
(16, '', 'kuyek', 'kuy@gmail.com', 98765432, 'kuyek', '789382b10686c37be64798cb99283468');

-- --------------------------------------------------------

--
-- Table structure for table `studio`
--

CREATE TABLE `studio` (
  `idstudio` int(11) NOT NULL,
  `nostudio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studio`
--

INSERT INTO `studio` (`idstudio`, `nostudio`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `idtiket` int(11) NOT NULL,
  `idnonton` int(11) DEFAULT NULL,
  `idpembeli` int(11) DEFAULT NULL,
  `idkursi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`idtiket`, `idnonton`, `idpembeli`, `idkursi`) VALUES
(1, 5, 3, 1),
(2, 2, 4, 6),
(3, 1, 2, 3),
(4, 4, 1, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`idfilm`),
  ADD KEY `idkategori` (`idkategori`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`idjadwal`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idkategori`);

--
-- Indexes for table `kursi`
--
ALTER TABLE `kursi`
  ADD PRIMARY KEY (`idkursi`),
  ADD KEY `fkstudio` (`idstudio`);

--
-- Indexes for table `nonton`
--
ALTER TABLE `nonton`
  ADD PRIMARY KEY (`idnonton`),
  ADD KEY `fk_studio` (`idstudio`),
  ADD KEY `fkfilm` (`idfilm`),
  ADD KEY `fkjadwal` (`idjadwal`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`idnota`),
  ADD KEY `idpembeli` (`idpembeli`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`idpembeli`);

--
-- Indexes for table `studio`
--
ALTER TABLE `studio`
  ADD PRIMARY KEY (`idstudio`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`idtiket`),
  ADD KEY `fknonton` (`idnonton`),
  ADD KEY `fkpembeli` (`idpembeli`),
  ADD KEY `fkkursi` (`idkursi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `idfilm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `idjadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kursi`
--
ALTER TABLE `kursi`
  MODIFY `idkursi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nonton`
--
ALTER TABLE `nonton`
  MODIFY `idnonton` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `idpembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `studio`
--
ALTER TABLE `studio`
  MODIFY `idstudio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `idtiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kursi`
--
ALTER TABLE `kursi`
  ADD CONSTRAINT `fkstudio` FOREIGN KEY (`idstudio`) REFERENCES `studio` (`idstudio`);

--
-- Constraints for table `nonton`
--
ALTER TABLE `nonton`
  ADD CONSTRAINT `fk_studio` FOREIGN KEY (`idstudio`) REFERENCES `studio` (`idstudio`),
  ADD CONSTRAINT `fkfilm` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`),
  ADD CONSTRAINT `fkjadwal` FOREIGN KEY (`idjadwal`) REFERENCES `jadwal` (`idjadwal`);

--
-- Constraints for table `nota`
--
ALTER TABLE `nota`
  ADD CONSTRAINT `nota_ibfk_1` FOREIGN KEY (`idpembeli`) REFERENCES `pembeli` (`idpembeli`);

--
-- Constraints for table `tiket`
--
ALTER TABLE `tiket`
  ADD CONSTRAINT `fkkursi` FOREIGN KEY (`idkursi`) REFERENCES `kursi` (`idkursi`),
  ADD CONSTRAINT `fknonton` FOREIGN KEY (`idnonton`) REFERENCES `nonton` (`idnonton`),
  ADD CONSTRAINT `fkpembeli` FOREIGN KEY (`idpembeli`) REFERENCES `pembeli` (`idpembeli`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
